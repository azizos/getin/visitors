# visitors API

## visitors API

As a part of the HappyQ application, the visitors API, written in Go, is used to view, register or modify visitors linked to the HappyQ application.

### What does this service do?

The service offers several HTTP endpoints (described below) using [Gin](https://github.com/gin-gonic/gin) framework. To persist data, we're using a Mongo DB database.

#### Available endpoints

| Method | Path               | Description                                  |
| ------ | ------------------ | -------------------------------------------  |
| GET    | /visitors        | To retrieve a list of visitors             |
| POST   | /visitors        | To add a new visitor                        |
| GET    | /visitors/[id]   | To retrieve a visitor by its __id_ property |
| PUT    | /visitors/[id]   | To update a visitor by its _id property     |
| DELETE | /visitors/[id]   | To delete a visitor by its _id property     |

#### Postman collection

*todo*

## Run
To run this service locally, you can use `docker-compose` (recommended) using `docker-compose.yml` file available in this repository. 

Run the command below in this project's root directory and the API is locally available on port 8080 (by default) :)
-`docker-compose up --build --remove-orphans`