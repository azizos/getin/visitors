package models

// Visitor struct is used to store and parse visitor objects
type Visitor struct {
	ID           string `json:"_id,omitempty" bson:"_id,omitempty" `
	Name         string `bson:"name" json:"name"`
	Email        string `bson:"email" json:"email"`
	CreationDate string `bson:"added_on" json:"added_on"`
}
