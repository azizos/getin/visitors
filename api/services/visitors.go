package services

import (
	"context"
	"time"

	"dev.azure.com/happyq/HappyQ/_git/visitors/api/common"
	"dev.azure.com/happyq/HappyQ/_git/visitors/api/models"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
)

var log, _ = common.GetLogModule()
var ctx context.Context
var ctxCancelFunc context.CancelFunc
var timeTillContextDeadline = 2 * time.Second

// ListVisitors retrieves all visitor objects from the database
func ListVisitors(collection *mongo.Collection) []*models.Visitor {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	var visitors []*models.Visitor
	cursor, err := collection.Find(ctx, bson.M{})
	defer cursor.Close(ctx)
	if err != nil {
		log.Debug("No visitors returned from the database:", err)
	} else {
		for cursor.Next(context.TODO()) {
			var result *models.Visitor
			err := cursor.Decode(&result)
			if err != nil {
				log.Debug("Problem retrieving visitor:", err)
			} else {
				visitors = append(visitors, result)
			}
		}
	}

	return visitors
}

// StoreVisitor stores a new visitor
func StoreVisitor(collection *mongo.Collection, newvisitor *models.Visitor) *models.Visitor {
	var result *models.Visitor
	if visitorExists(collection, newvisitor.Email) {
		return nil
	}
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	newvisitor.CreationDate = time.Now().Format("2006-01-02")
	res, err := collection.InsertOne(ctx, &newvisitor)
	if err != nil {
		log.Errorf("Problem saving a visitor: %v", err)
	}

	err = collection.FindOne(ctx, bson.M{"_id": res.InsertedID}).Decode(&result)
	if err != nil {
		log.Errorf("Problem finding a visitor: %v", err)
	}
	log.Infof("New visitor %v has been added", res.InsertedID)

	return result
}

// GetVisitor finds one single visitor object based on id
func GetVisitor(collection *mongo.Collection, id string) *models.Visitor {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	var result *models.Visitor
	objectID, _ := primitive.ObjectIDFromHex(id)
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&result)
	if err != nil {
		log.Errorf("Problem finding a visitor: %v", err)
	}
	return result
}

// UpdateVisitor finds one single visitor object based on id
func UpdateVisitor(collection *mongo.Collection, id string, newvisitor *models.Visitor) *models.Visitor {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	objectID, _ := primitive.ObjectIDFromHex(id)
	filter := bson.M{"_id": objectID}
	update := bson.M{
		"$set": bson.M{"name": newvisitor.Name},
	}
	upsert := true
	after := options.After
	opt := options.FindOneAndUpdateOptions{
		ReturnDocument: &after,
		Upsert:         &upsert,
	}
	result := collection.FindOneAndUpdate(ctx, filter, update, &opt)
	if result.Err() != nil {
		log.Errorf("Problem removing a visitor: %v: %v", objectID, result.Err)
	}
	err := collection.FindOne(ctx, bson.M{"_id": objectID}).Decode(&newvisitor)
	if err != nil {
		log.Errorf("Problem finding a visitor: %v", err)
	}
	return newvisitor
}

// DeleteVisitor delete one single visitor object based on id
func DeleteVisitor(collection *mongo.Collection, id string) error {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()
	objectID, _ := primitive.ObjectIDFromHex(id)

	res := collection.FindOneAndDelete(ctx, bson.M{"_id": objectID})
	log.Infof("visitor %v has been deleted", id)
	if res.Err() != nil {
		log.Errorf("Problem removing a visitor: %v: %v", objectID, res.Err)
	}
	return res.Err()
}

func visitorExists(collection *mongo.Collection, email string) bool {
	ctx, ctxCancelFunc = context.WithTimeout(context.Background(), timeTillContextDeadline)
	defer ctxCancelFunc()

	filter := bson.M{"email": email}

	err := collection.FindOne(ctx, filter).Err()
	if err == nil {
		log.Infof("Visitor with email '%v' already exists in the database and cannot be added", email)
		return true
	}
	return false
}
