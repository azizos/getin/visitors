package api

import (
	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"go.mongodb.org/mongo-driver/mongo"
)

// GetRoutes defines and returns a list of routes to be used by the main HTTP handler
func GetRoutes(route *gin.Engine, db *mongo.Collection) {
	check := Check{}

	route.Use(gin.Recovery())
	route.Use(databaseMiddleware(db))

	visitors := route.Group("/visitors")
	{
		visitors.GET("/", List)
		visitors.POST("/", Store)
		visitors.GET("/:id", Get)
		visitors.PUT("/:id", Update)
		visitors.DELETE("/:id", Delete)
	}
	route.GET("/health", check.Health)
	route.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
}

func databaseMiddleware(db *mongo.Collection) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Set("db", db)
		c.Next()
	}
}
