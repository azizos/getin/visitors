//todo: add swagger!

package api

import (
	"dev.azure.com/happyq/HappyQ/_git/visitors/api/common"
	"dev.azure.com/happyq/HappyQ/_git/visitors/api/models"
	"dev.azure.com/happyq/HappyQ/_git/visitors/api/services"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/gin-gonic/gin"
)

var log, _ = common.GetLogModule()

// List godoc
// @Summary Show all visitors
// @Description get all existing visitors
// @ID get-all-visitors
// @Accept  json
// @Produce  json
// @Success 200 {array} models.Visitor
// @Failure 204
// @Router /visitors/ [get]
func List(context *gin.Context) {
	db := context.MustGet("db").(*mongo.Collection)
	visitors := services.ListVisitors(db)
	if len(visitors) <= 0 {
		context.AbortWithStatus(204)
		return
	}
	context.JSON(200, visitors)
	return
}

// Store is used to insert a visitor object
func Store(context *gin.Context) {
	var visitor *models.Visitor
	db := context.MustGet("db").(*mongo.Collection)
	if err := context.ShouldBindJSON(&visitor); err != nil {
		context.AbortWithStatus(400)
		return
	}
	changedvisitor := services.StoreVisitor(db, visitor)
	if changedvisitor != nil {
		context.JSON(200, changedvisitor)
		return
	}

	context.AbortWithStatus(400)
	return
}

// Get is used to retrieve a visitor object by its id property: responding to /:id
func Get(context *gin.Context) {
	db := context.MustGet("db").(*mongo.Collection)
	id := context.Param("id")
	visitor := services.GetVisitor(db, id)
	if visitor == nil {
		context.AbortWithStatus(204)
		return
	}
	context.JSON(200, visitor)
	return
}

// Update is used to update a visitor object by its _id property
func Update(context *gin.Context) {
	var visitor *models.Visitor
	db := context.MustGet("db").(*mongo.Collection)
	id := context.Param("id")
	if err := context.ShouldBindJSON(&visitor); err != nil {
		context.AbortWithStatus(400)
		return
	}
	changedvisitor := services.UpdateVisitor(db, id, visitor)
	if changedvisitor == nil {
		context.AbortWithStatus(404)
		return
	}
	context.JSON(200, changedvisitor)
	return
}

// Delete is used to update a visitor object by its _id property
func Delete(context *gin.Context) {
	db := context.MustGet("db").(*mongo.Collection)
	id := context.Param("id")
	err := services.DeleteVisitor(db, id)
	if err != nil {
		context.AbortWithStatus(400)
		return
	}
	context.Status(200)
	return
}
