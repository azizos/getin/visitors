package main

import (
	"context"
	"fmt"
	"os"

	"dev.azure.com/happyq/HappyQ/_git/visitors/api/common"
	"dev.azure.com/happyq/HappyQ/_git/visitors/api/docs"
	api "dev.azure.com/happyq/HappyQ/_git/visitors/api/web"
	"github.com/ardanlabs/conf"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"go.mongodb.org/mongo-driver/mongo"
)

var dbCollectionName string = "visitors"
var log, _ = common.GetLogModule()
var db *mongo.Database
var ctx context.Context

func main() {
	if err := run(); err != nil {
		log.Error("Service cannot be started: ", err)
		os.Exit(1)
	}
}

func run() error {
	var cfg struct {
		MongoURI string `conf:"default:mongodb://localhost:27017/"`
		MongoDB  string `conf:"default:happyq"`
		APIHost  string `conf:"default:0.0.0.0:8080"`
		GinMode  string `conf:"default:debug"`
	}

	if err := conf.Parse(os.Args[1:], "Visitors", &cfg); err != nil {
		if err == conf.ErrHelpWanted {
			usage, err := conf.Usage("Visitors", &cfg)
			if err != nil {
				log.Error("Generating config usage: ", err)
			}
			fmt.Println(usage)
			return nil
		}
		return errors.Wrap(err, "Problem parsing api configs")
	}

	defer log.Info("Main: completed")

	db = common.ConnectDB(cfg.MongoURI, cfg.MongoDB)
	collection := db.Collection(dbCollectionName)

	gin.SetMode(cfg.GinMode)
	docs.SwaggerInfo.Title = "Visitors API"
	docs.SwaggerInfo.Description = "Visitors API is a part of HappyQ project"
	router := gin.Default()
	api.GetRoutes(router, collection)

	err := router.Run(cfg.APIHost)
	return err
}
