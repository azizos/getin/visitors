module dev.azure.com/happyq/HappyQ/_git/visitors

go 1.13

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/ardanlabs/conf v1.2.1
	github.com/gin-gonic/gin v1.5.0
	github.com/go-openapi/spec v0.19.8 // indirect
	github.com/go-openapi/swag v0.19.9 // indirect
	github.com/mailru/easyjson v0.7.1 // indirect
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/pkg/errors v0.9.1
	github.com/swaggo/gin-swagger v1.2.0
	github.com/swaggo/swag v1.6.5
	go.mongodb.org/mongo-driver v1.3.2
	golang.org/x/net v0.0.0-20200513185701-a91f0712d120 // indirect
	golang.org/x/tools v0.0.0-20200515010526-7d3b6ebf133d // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
